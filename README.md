# 16 - Monitoring with Prometheus - Configure Monitoring for Own Application
# Collect & Expose Metrics with Prometheus Client Library
# Scrape Own Application Metrics and Configure Own Grafana Dashboard

**Demo Project:**
Configure Monitoring for Own Application

**Technologies used:**
Prometheus, Kubernetes, Node.js, Grafana, Docker, Docker Hub

**Project Description:**
- Configure our NodeJS application to collect & expose Metrics with Prometheus Client Library 
- Deploy the NodeJS application, which has a metrics endpoint configured, into Kubernetes cluster 
- Configure Prometheus to scrape this exposed metrics and visualize it in Grafana Dashboard



16 - Monitoring with Prometheus
# This project was developed as part of the DevOps Bootcamp at Techword With NANA https://www.techworld-with-nana.com/devops-bootcamp





